package main

import (
    "bufio"
    "flag"
    "fmt"
    "io/ioutil"
    "log"
    "os"
    "regexp"
)

func scanEmail(filename string) []string {
    //Open the e-mail.
    //TODO: Search the specified directory after files and then open them for read.
    email, err := os.Open(filename)
    if err != nil {
        log.Fatal(err)
    }
    defer email.Close()

    //Start scanner
    scanner := bufio.NewScanner(email)

    //Slice to store trackIDs found
    var IDs []string

    //Loop over all lines in the file
    for scanner.Scan() {
        scan := scanner.Text()

        //Regexp to find trackIDs. I.e. 0056754839
        match, _ := regexp.MatchString("00\\d{8}", scan)
        r, _ := regexp.Compile("00\\d{8}")

        if match == true {
            trackID := r.FindString(scan)
            IDs = append(IDs, trackID)
        }
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }
    return IDs
}

func main() {
    dir := flag.String("d", "", "Specify the directory containing the e-mails.")
    flag.Parse()

    //Search string for different systems
    var BIS string
    var MT string
    var counter int

    //Read files in specified directory and pass it to scanEmail
    files, _ := ioutil.ReadDir(*dir)
    for _, f := range files {
        trackIDs := scanEmail(*dir + f.Name())

        var current string

        for _, trackID := range trackIDs {
            if trackID != current {
                current = trackID
                BIS += trackID + " "
                MT += trackID + ";"
                counter++
            }
        }
    }
    fmt.Println("BIS Search: " + BIS + "\n\n")
    fmt.Printf("Message tracker search:\n[%s]\n\n", MT)
    fmt.Printf("TrackIDs found: %d\n", counter)
}
