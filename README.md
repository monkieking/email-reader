## E-mail reader

Just a little app that can read files and find a specific ID using regexp.
Made it for easier times at my work :)

**Usage**:
./app -h
  -d string
        Specify the directory containing the e-mails.

